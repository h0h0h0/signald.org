---
title: Chat
aliases:
    - /articles/IRC/
---

signald's community primarily exists in a chat room. There are a few ways to get to the chat room:

* **IRC** server: `libera.chat` channel: `#signald` ([link](irc://irc.libera.chat/#signald))
* **Matrix** server: `libera.chat` channel: `#signald` ([link](https://matrix.to/#/#signald:libera.chat))
* **Web**: [link](https://web.libera.chat/#signald)