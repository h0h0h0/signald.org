The docker image can be pulled from:
* docker hub: `signald/signald`
* GitLab's container registry: `registry.gitlab.com/signald/signald`

signald stores both account state files and the unix socket file in `/signald`. Mount it as a volume:

```bash
docker run -v $(pwd)/run:/signald signald/signald
```

The image supports amd64, armv7 and armv8

## Additional tooling

Consider installing [signaldctl](/signaldctl/).

## Docker for Mac

_Docker for Mac_ does not allow you to pass sockets via docker volumes. [This Docker issue](https://github.com/docker/for-mac/issues/483) sums it up at the end. Attempts to connect to the mounted socket result in `Connection Refused` errors.

