---
title: Protocol Versioning
aliases:
    - /articles/protocol-versioning/
---

In order to allow for protocol changes without breaking backwards compatibility, signald request types have a version associated
with them. This is specified with the `"version"` key in the JSON request:

```json
{"type": "example", "version": "v1"}
```

Multiple versions of the same request type may exist at the same time, to allow clients time to update when things change in a
way that cannot be made backwards compatible.

All clients should include a `version` in all requests. When a request with no version comes in, signald will print a debug-level
log line will be printed (run signald with `-v` to see these lines)

All request types before this change have been dubbed `v0`. All functionality of v0 protocol is available in v1. Existing
clients are encouraged to update, as the v0 protocol is going away at the end of 2021. New clients should only use v1+
requests. Internally, the request handling process for v1+ is much more robust and has better error messaging. Additionally,
documentation on v1+ requests, responses and associated data types are available via the
[machine-readable protocol documentation](/articles/protocol/documentation/), and documented on this site (see Protocol
header in the sidebar).

Work on this protocol is ongoing. Feedback should be directed to [the issue tracker](https://gitlab.com/signald/signald/-/issues)
or [the chat room](/articles/chat/)

## Deprecation

Eventually old versions of request types will be removed. When signald receives a request with a deprecated version of that request
type, a warn-level log line will be emitted.

* `alpha` versions may be removed within one minor version of being deprecated (that is, versions with the word `alpha` in the name,
    such as `v1alpha`)
* major versions (with no alpha or beta suffix) will be kept around for 6 months to a year after they are deprecated. Exact timeline
    will be announced on an individual basis.

All v0 requests have been deprecated and will be removed at the end of 2021. 