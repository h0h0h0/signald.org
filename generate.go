package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"text/template"
)

var (
	CommitHash = os.Getenv("CI_COMMIT_SHA")
)

type Protocol struct {
	Types   map[string]map[string]*Type
	Actions map[string]map[string]*Action
}

type Type struct {
	Fields     map[string]*DataType
	Request    bool `json:"-"`
	Doc        string
	Deprecated bool
	Error      bool
}

type DataType struct {
	List    bool
	Type    string
	Version string
	Doc     string
	Example string
}

type Action struct {
	FnName        string
	Request       string
	RequestFields map[string]*DataType
	Response      string
	Doc           string
	Deprecated    bool
	Errors        []Error
}

type Error struct {
	Name string
	Doc  string
}

type StructureTemplateInput struct {
	Name       string
	Type       *Type
	Version    string
	References []TypeReference
	Protocol   Protocol
	CommitHash string
}

type ActionTemplateInput struct {
	Action     *Action
	Type       string
	Version    string
	Protocol   Protocol
	CommitHash string
	Example    string
}

type IndexTemplateInput struct {
	Title           string
	CollapseSection bool
}

type TypeReference struct {
	Name    string
	Fields  []string
	Version string
}

func main() {
	var response Protocol
	err := json.NewDecoder(os.Stdin).Decode(&response)
	if err != nil {
		log.Fatal(err, "\nError parsing stdin")
	}

	t, err := template.ParseGlob("templates/*.md")
	if err != nil {
		log.Fatal(err, "\nError parsing templates from templates/*.md")
	}

	for version, types := range response.Types {
		outputDir := fmt.Sprintf("content/protocol/structures/%s", version)
		err = os.MkdirAll(outputDir, os.ModePerm)
		if err != nil {
			log.Fatal("Error creating", outputDir, err)
		}

		err = generateIndex(t, outputDir, IndexTemplateInput{Title: version, CollapseSection: true})
		if err != nil {
			log.Fatal("error rendering version template: ", err)
		}
		for typeName, ty := range types {
			input := StructureTemplateInput{
				Name:       typeName,
				Version:    version,
				Type:       ty,
				Protocol:   response,
				References: FindReferences(version, typeName, response.Types),
				CommitHash: CommitHash,
			}
			outputFilename := fmt.Sprintf("%s/%s.md", outputDir, typeName)
			log.Println("writing", outputFilename)
			f, err := os.Create(outputFilename)
			if err != nil {
				log.Fatal(err, "failed to open output file ", outputFilename)
			}
			err = t.ExecuteTemplate(f, "structure.md", input)
			if err != nil {
				log.Fatal(err, "\nfailed to render template")
			}
			log.Println("successfully wrote", outputFilename)
		}
	}

	err = generateIndex(t, "content/protocol/structures", IndexTemplateInput{Title: "structures", CollapseSection: true})
	if err != nil {
		log.Fatal("error rendering version template: ", err)
	}

	for version, actions := range response.Actions {
		outputDir := fmt.Sprintf("content/protocol/actions/%s", version)
		err = os.MkdirAll(outputDir, os.ModePerm)
		if err != nil {
			log.Fatal("Error creating", outputDir, err)
		}
		err = generateIndex(t, outputDir, IndexTemplateInput{Title: version, CollapseSection: true})
		if err != nil {
			log.Fatal("error rendering version template: ", err)
		}

		for actionType, action := range actions {
			example := generateExample(response.Types[version][action.Request], response.Types)
			example["version"] = version
			example["type"] = actionType
			exampleBuffer, err := json.MarshalIndent(example, "", "    ")
			if err != nil {
				log.Fatal("unable to JSONify example value:", example, err)
			}
			input := ActionTemplateInput{
				Action:     action,
				Type:       actionType,
				Example:    string(exampleBuffer),
				Version:    version,
				Protocol:   response,
				CommitHash: CommitHash,
			}
			outputFilename := fmt.Sprintf("%s/%s.md", outputDir, actionType)
			log.Println("writing", outputFilename)
			f, err := os.Create(outputFilename)
			if err != nil {
				log.Fatal(err, "failed to open output file ", outputFilename)
			}
			err = t.ExecuteTemplate(f, "action.md", input)
			if err != nil {
				log.Fatal(err, "\nfailed to render template")
			}
			log.Println("successfully wrote", outputFilename)
		}
	}

	err = generateIndex(t, "content/protocol/actions", IndexTemplateInput{Title: "requests", CollapseSection: true})
	if err != nil {
		log.Fatal("error rendering version template: ", err)
	}

	err = generateIndex(t, "content/signaldctl/reference", IndexTemplateInput{Title: "man pages", CollapseSection: true})
	if err != nil {
		log.Fatal("error rendering version template: ", err)
	}
}

func FindReferences(version, t string, allTypes map[string]map[string]*Type) []TypeReference {
	references := []TypeReference{}
	for v, types := range allTypes {
		for typeName, ty := range types {
			for fieldName, field := range ty.Fields {
				if field.Version == version && field.Type == t {
					exists := false
					for i, existing := range references {
						if existing.Version == version && existing.Name == typeName {
							references[i].Fields = append(references[i].Fields, fieldName)
							exists = true
							break
						}
					}
					if !exists {
						references = append(references, TypeReference{Name: typeName, Fields: []string{fieldName}, Version: v})
					}
				}
			}
		}
	}
	return references
}

func generateExample(t *Type, allTypes map[string]map[string]*Type) map[string]interface{} {
	out := make(map[string]interface{})
	for fieldName, fieldType := range t.Fields {
		var value interface{}
		if fieldType.Example != "" {
			err := json.Unmarshal([]byte(fieldType.Example), &value)
			if err != nil {
				log.Fatal("Error unmarshalling value: ", fieldType.Example, err)
			}
		} else if fieldType.Version != "" {
			possible := generateExample(allTypes[fieldType.Version][fieldType.Type], allTypes)
			if len(possible) > 0 {
				value = possible
			}
		}

		if value != nil {
			if fieldType.List {
				out[fieldName] = []interface{}{value}
			} else {
				out[fieldName] = value
			}
		}
	}
	return out
}

func generateIndex(t *template.Template, outputDir string, values IndexTemplateInput) error {
	outputFilename := fmt.Sprintf("%s/_index.md", outputDir)
	log.Println("writing", outputFilename)
	f, err := os.Create(outputFilename)
	if err != nil {
		return err
	}
	err = t.ExecuteTemplate(f, "index.md", values)
	if err != nil {
		log.Fatal(err, "\nfailed to render template")
	}
	return nil
}
